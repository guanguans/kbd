<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html lang="zh-CN">

<head>

    <title><?php echo ($site_seo_title); ?> <?php echo ($site_name); ?></title>

    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />

    <meta name="description" content="<?php echo ($site_seo_description); ?>">

    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/kbd/themes/html/Public/js/jquery.min.js"></script>
<script src="/kbd/themes/html/Public/js/jquery.SuperSlide.2.1.1.js"></script>
<link href="/kbd/themes/html/Public/css/css.css" rel="stylesheet">

</head>

<body>

<div class="header">
    <div class="top">
        <div class="logo">
            <h1><a href="#">上海凯倍达电子科技有限公司</a></h1>
        </div>
        <div class="search-box">
            <div class="search" >
                <form method="post" class="form-inline" action="<?php echo U('portal/search/index');?>" style="margin:18px 0;" id="myform">
                    <input type="text" name="keyword" id="search" onfocus="myclear();">
                    <input type="button" value="站内搜索" onclick="mysubmit();" style="cursor: pointer;">
                </form>
            </div>
        </div>
        <script>
            function mysubmit(){
                var content = $("#search").val();
                var myform = $("#myform");
                if(content != '' &&  content != '请输入搜索内容') {
                    myform.submit();
                }else {
                   $("#search").val('请输入搜索内容');
                }
            }
            function myclear(){
               $("#search").val('');
            }
        </script>
    </div>
    <?php
 $date = date('Y.m.d',time()); $xq = date('N',time()); switch ($xq) { case '1': $xq = '星期一'; break; case '2': $xq = '星期二'; break; case '3': $xq = '星期三'; break; case '4': $xq = '星期四'; break; case '5': $xq = '星期五'; break; case '6': $xq = '星期六'; break; case '7': $xq = '星期日'; break; } $ch = curl_init(); $ip = get_client_ip(); $ip = '59.108.49.35'; $url = 'http://api.map.baidu.com/location/ip?ip='.$ip.'&ak=txCwnMDSGj2TKK6FB9yTn2nV9pdUMnSu&coor=bd09ll'; curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch , CURLOPT_URL , $url); $res = curl_exec($ch); $data = json_decode($res,true); if(isset($data['address']) && $data['address']){ $address_arr = explode('|',$data['address']); if(isset($address_arr[2]) && $address_arr[2] != 'None'){ $address = $address_arr[2]; } else { $address = '北京'; } } else { $address = '北京'; } $url = 'http://api.map.baidu.com/telematics/v3/weather?location='.$address.'&output=json&ak=txCwnMDSGj2TKK6FB9yTn2nV9pdUMnSu'; curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch , CURLOPT_URL , $url); $res = curl_exec($ch); $data2 = json_decode($res,true); $weather = $data2['results'][0]['weather_data'][0]['weather']; $city = $data2['results'][0]['currentCity']; ?>
    <div style="height:58px;float:right; background-color:#4353B1;padding-right:10px; line-height:60px;">
        <?php switch($weather): case "晴": ?><img src="/kbd/themes/html/Public/images/qing.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云": ?><img src="/kbd/themes/html/Public/images/duoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "晴转多云": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "晴转阴": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云转晴": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云转阴": ?><img src="/kbd/themes/html/Public/images/duoyunzhuanyin.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "阴": ?><img src="/kbd/themes/html/Public/images/yin.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "小雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "大雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "小雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "大雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php default: ?><div style="height:58px;float:right; line-height:58px;color:#fff;font-size:28px;"><?php echo ($weather); ?></div><?php endswitch;?>
        <div style="height:30px;float:right;padding-right:5px;">
            <div style='line-height:20px; color:#fff;margin-right:5px;'><?php echo ($date); ?></div>
            <div style='line-height:16px; color:#fff;margin-right:5px;text-align:right;'><?php echo ($xq); ?></div>
            <b style='color:#fff;height:30px;line-height:26px;float:right; margin-right:5px;'><?php echo ($city); ?></b>
            <img src="/kbd/themes/html/Public/images/position.jpg" style="float:right;height:20px; margin-top:4px; margin-right:5px;">
        </div>
    </div>
    <div class="nav clearfix" style="order:1px solid red; positon:relative;">
        <?php
 $effected_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='dropdown-toggle' data-toggle='dropdown'>\$label <b class='caret'></b></a>"; $ul_class="dropdown-menu" ; $li_class="" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
        <!--<ul>-->
            <!--<li><a href="#">首页</a></li>-->
            <!--<li><a href="#">产品介绍</a></li>-->
            <!--<li><a href="#">技术天地</a></li>-->
            <!--<li><a href="#">应用案例</a></li>-->
            <!--<li><a href="#">关于我们</a></li>-->
        <!--</ul>-->
    </div>

    </div>

    <div class="crumb">

        您的当前位置：<a href="/kbd/">首页</a>&nbsp;&nbsp;>&nbsp;&nbsp;

        <?php echo ($name); ?>

    </div>

    <div class="main-box">

        <div class="doc-box clearfix">

            <div class="doc-right-list anli-list">

                <h3><?php echo ($name); ?></h3>

                <?php $lists = sp_sql_posts_paged("cid:$cat_id;order:post_date DESC;",10); ?>

                <ul>

                    <?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>"><?php echo ($vo["post_title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>

                </ul>

            </div>

        </div>

        <div class="anli-pro">

            <div class="anli-pro-list clearfix" >

                <h3>应用照片</h3>

                <?php $home_slides=sp_getslide("anli-pro",5); ?>

                <ul>

                    <?php if(is_array($home_slides)): foreach($home_slides as $key=>$vo): if($key == 3): ?><li class="n-m-r">

                            <?php else: ?>

                            <li><?php endif; ?>



                        <a href="<?php echo ($vo["slide_url"]); ?>"><img src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" alt=""></a>

                        <p><a href="#"><?php echo ($vo["slide_name"]); ?></a></p>

                    </li><?php endforeach; endif; ?>



                </ul>

            </div>



        </div>

    </div>

<div class="footer">
    <p>上海凯倍达电子科技有限公司 版权所有   技术支持：慧聪国际资讯 慧聪行业门户网</p>
</div>

</body>

</html>