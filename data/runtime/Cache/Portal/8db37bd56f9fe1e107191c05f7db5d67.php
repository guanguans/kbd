<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title><?php echo ($site_seo_title); ?> <?php echo ($site_name); ?></title>
    <meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />
    <meta name="description" content="<?php echo ($site_seo_description); ?>">
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="/kbd/themes/html/Public/js/jquery.min.js"></script>
<script src="/kbd/themes/html/Public/js/jquery.SuperSlide.2.1.1.js"></script>
<link href="/kbd/themes/html/Public/css/css.css" rel="stylesheet">
</head>
<body>
<div class="header">
    <div class="top">
        <div class="logo">
            <h1><a href="#">上海凯倍达电子科技有限公司</a></h1>
        </div>
        <div class="search-box">
            <div class="search" >
                <form method="post" class="form-inline" action="<?php echo U('portal/search/index');?>" style="margin:18px 0;" id="myform">
                    <input type="text" name="keyword" id="search" onfocus="myclear();">
                    <input type="button" value="站内搜索" onclick="mysubmit();" style="cursor: pointer;">
                </form>
            </div>
        </div>
        <script>
            function mysubmit(){
                var content = $("#search").val();
                var myform = $("#myform");
                if(content != '' &&  content != '请输入搜索内容') {
                    myform.submit();
                }else {
                   $("#search").val('请输入搜索内容');
                }
            }
            function myclear(){
               $("#search").val('');
            }
        </script>
    </div>
    <?php
 $date = date('Y.m.d',time()); $xq = date('N',time()); switch ($xq) { case '1': $xq = '星期一'; break; case '2': $xq = '星期二'; break; case '3': $xq = '星期三'; break; case '4': $xq = '星期四'; break; case '5': $xq = '星期五'; break; case '6': $xq = '星期六'; break; case '7': $xq = '星期日'; break; } $ch = curl_init(); $ip = get_client_ip(); $url = 'http://api.map.baidu.com/location/ip?ip='.$ip.'&ak=txCwnMDSGj2TKK6FB9yTn2nV9pdUMnSu&coor=bd09ll'; curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch , CURLOPT_URL , $url); $res = curl_exec($ch); $data = json_decode($res,true); if(isset($data['address']) && $data['address']){ $address_arr = explode('|',$data['address']); if(isset($address_arr[2]) && $address_arr[2] != 'None'){ $address = $address_arr[2]; } else { $address = '北京'; } } else { $address = '北京'; } $url = 'http://api.map.baidu.com/telematics/v3/weather?location='.$address.'&output=json&ak=txCwnMDSGj2TKK6FB9yTn2nV9pdUMnSu'; curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch , CURLOPT_URL , $url); $res = curl_exec($ch); $data2 = json_decode($res,true); $weather = $data2['results'][0]['weather_data'][0]['weather']; $city = $data2['results'][0]['currentCity']; ?>
    <div style="height:58px;float:right; background-color:#4353B1;padding-right:10px; line-height:60px;">
        <?php switch($weather): case "晴": ?><img src="/kbd/themes/html/Public/images/qing.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云": ?><img src="/kbd/themes/html/Public/images/duoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "晴转多云": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "晴转阴": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云转晴": ?><img src="/kbd/themes/html/Public/images/qingzhuanduoyun.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "阵雨转多云": ?><img src="/kbd/themes/html/Public/images/duoyunzhuanyin.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "多云转阴": ?><img src="/kbd/themes/html/Public/images/duoyunzhuanyin.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "阴": ?><img src="/kbd/themes/html/Public/images/yin.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "小雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "大雨": ?><img src="/kbd/themes/html/Public/images/yu.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "小雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php case "大雪": ?><img src="/kbd/themes/html/Public/images/xue.jpg" alt="<?php echo ($weather); ?>" style="float:right;margin-top:9px;"><?php break;?>
            <?php default: ?><div style="height:58px;float:right; line-height:58px;color:#fff;font-size:28px;"><?php echo ($weather); ?></div><?php endswitch;?>
        <div style="height:30px;float:right;padding-right:5px;">
            <div style='line-height:20px; color:#fff;margin-right:5px;'><?php echo ($date); ?></div>
            <div style='line-height:16px; color:#fff;margin-right:5px;text-align:right;'><?php echo ($xq); ?></div>
            <b style='color:#fff;height:30px;line-height:26px;float:right; margin-right:5px;'><?php echo ($city); ?></b>
            <img src="/kbd/themes/html/Public/images/position.jpg" style="float:right;height:20px; margin-top:4px; margin-right:5px;">
        </div>
    </div>
    <div class="nav clearfix" style="order:1px solid red; positon:relative;">
        <?php
 $effected_id="main-menu"; $filetpl="<a href='\$href' target='\$target'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='dropdown-toggle' data-toggle='dropdown'>\$label <b class='caret'></b></a>"; $ul_class="dropdown-menu" ; $li_class="" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
        <!--<ul>-->
            <!--<li><a href="#">首页</a></li>-->
            <!--<li><a href="#">产品介绍</a></li>-->
            <!--<li><a href="#">技术天地</a></li>-->
            <!--<li><a href="#">应用案例</a></li>-->
            <!--<li><a href="#">关于我们</a></li>-->
        <!--</ul>-->
    </div>

    </div>
<div class="main clearfix">
    <div class="main-left">
        <div class="category-top clearfix">
           <!-- <div class="category">
                <h3 class="category-title category-color-1">NEWS</h3>
                <?php $lists = sp_sql_posts_paged("cid:24;order:post_date DESC;where:recommended=1;limit:0,2",0); ?>
                <div class="news clearfix">
                    <div class="news-recommend-1 clearfix">
                        <?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'],true); ?>
                        <dl class="clearfix">

                            <dt><a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>" target="_blank"><img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt=""></a></dt>
                            <?php if($key == 1): ?><dd class="n-m-b">
                                    <?php else: ?>
                                    <dd class=""><?php endif; ?>


                                <a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>" target="_blank"><?php echo ($vo["post_excerpt"]); ?></a></dd>
                        </dl><?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                    <div class="news-recommend-2 clearfix">
                        <?php $lists = sp_sql_posts_paged("cid:24;order:post_date DESC;where:recommended=1;limit:2,1",0); ?>
                        <?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'],true); ?>
                        <dl>
                            <dt><a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>" target="_blank"><img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt=""></a></dt>
                            <dd>
                                <a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>" target="_blank"><?php echo ($vo["post_excerpt"]); ?></a></dd>
                        </dl><?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>

                </div>
            </div>  -->
            <div class="category category-news">
                <h3 class="category-title category-color-1">NEWS</h3>
                <div class="category-content-1">
                    <?php $lists = sp_sql_posts_paged("cid:24;order:post_date DESC;where:recommended=1;limit:0,2",0); $smeta=json_decode($lists['posts'][0]['smeta'],true); ?>
                    <li></li>
                    <a href="<?php echo leuu('article/index',array('id'=>$lists['posts'][0]['object_id'],'cid'=>$lists['posts'][0]['term_id']));?>"><p class="fisrt-p"><?php echo ($lists['posts'][0]['post_excerpt']); ?></p></a>
                    <div>
                        <img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt="">  
                        <p class="last-p"><?php echo ($lists['posts'][0]['post_source']); ?></p>
                    </div>
                </div>
                <div class="dashed-line" style="border-top:1px dashed #ccc;width:400px;margin-left:20px;float:left;"></div>
                <div class="category-content-2">
                    <?php $lists = sp_sql_posts_paged("cid:24;order:post_date DESC;where:recommended=1;limit:2,1",0); $smeta=json_decode($lists['posts'][0]['smeta'],true); ?>
                    <li></li>
                    <a href="<?php echo leuu('article/index',array('id'=>$lists['posts'][0]['object_id'],'cid'=>$lists['posts'][0]['term_id']));?>"><p class="fisrt-p"><?php echo ($lists['posts'][0]['post_excerpt']); ?></p>
                    <img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt=""></a>  
                </div>
            </div>
            <style>
                .category-news{height:686px;}
                .category-news a:hover{color:#ff9000;}
                .category-content-1{float:left;width:100%;padding:20px 20px 15px 0px;box-sizing:border-box;}
                .category-content-1 li{float:left;width:30px;height:200px;background: url('/kbd/themes/html/Public/images/index-icon.png') center 5px no-repeat;}
                .category-content-1 .first-p{width:100%;float:left;}
                .category-content-1 div{margin-top: 10px;}
                .category-content-1 div img{float:left;width:190px;height:193px;}
                .category-content-1 div .last-p{color:#fff;width:190px;height:193px;float:left;padding:20px 10px;box-sizing:border-box;background:#ff9000;overflow:hidden;}
                .category-content-2{float:left;width:100%;padding:30px 20px 15px 0px;box-sizing:border-box;}
                .category-content-2 li{float:left;width:30px;height:150px;background: url('/kbd/themes/html/Public/images/index-icon.png') center 5px no-repeat;}
                .category-content-1 .first-p{width:100%;float:left;}
                .category-content-2 img{float:left;width:380px;margin-top: 15px;}
            </style>
            <div class="category-right">
                <h3 class="category-title category-color-2">Company</h3>
                <div class="company">
                    <div id="slideBox" class="slideBox">
                        <div class="bd">
                            <ul>
                                <?php $home_slides=sp_getslide("home_index",10); ?>
                                <?php if(is_array($home_slides)): foreach($home_slides as $key=>$vo): ?><li><a href="<?php echo ($vo["slide_url"]); ?>"><img src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" /></a></li><?php endforeach; endif; ?>

                            </ul>
                        </div>
                        <a class="prev" href="javascript:void(0)"></a>
                        <a class="next" href="javascript:void(0)"></a>
                    </div>
                    <div class="company-desc">
                        <p>“大浪淘沙，觅得真金”。产品及服务的质量是企业的生命，上海凯倍达电子科技有限公司成立于2002年，是一家技术型仪器仪表销售公司，致力于新产品及测试系统的推广和研发合作，提供专业的技术咨询和完美的售后服务，使各行各业的广大用户满意、信赖</p>
                        <p>我们是世界著名压力测量专家-瑞士KELLER公司的中国授权代理商，KELLER公司有压力变送器，差压变送器，投入式液位变送器，数字压力表，压力校验仪等产品。选择范围广，从经济型至校验级精度，适合于各类研发及工业应用。</p>
                        <p>我们是世界著名位移传感器生产厂家-美国Lion Precision雄狮精仪公司的中国授权代理商，雄狮公司的产品种类齐全，性能可靠，质量上乘。品种有电容位移传感器，电涡流位移传感器，主轴回转误差分析仪、电容式标签传感器等产品</p>
                        <p>同时我们还经销德国ME、白俄罗斯Riftek的激光位移传感器、二维激光轮廓扫描仪，以及瑞士Huba的陶瓷膜片压力变送器、微差压变送器等产品</p>
                        <p>如果您想进行技术革新，如果您想升级现有设备，如果您想完成试验任务，如果您想提高生产效率......甚至您只是想寻找使用方便的测量工具，您在选型中遇到任何问题，请与我们联系</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="category-bottom clearfix">
            <div class="category">
                <h3 class="category-title category-color-2">Contacts</h3>
                <div class="categroy-con">
                    <p>上海凯倍达电子科技有限公司</p>
                    <p>地址：上海市闵行区友情路50弄19号1002室</p>
                    <p>(距地铁1号线莘庄站南出口200米)</p>
                    <p>邮编：201100</p>
                    <p>电话：021-64606590 64603259</p>
                    <p>手机：13601932466</p>
                    <p>传真：021-64606306 64606590</p>
                    <p>E-mail：kbright@x263.net</p>
                    <p class="link-index"><a href="#">www.kbr<i>i</i>ght.com.cn</a></p>
                </div>
            </div>
            <div class="category-right">
                <h3 class="category-title category-color-3"><a href="<?php echo leuu('list/index',array('id'=>25));?>">更多</a>Notice</h3>
                <div class="categroy-con">
                    <ul>
                        <?php $lists = sp_sql_posts_paged("cid:25;order:post_date DESC;;limit:0,8",0); ?>

                        <?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                                <span>
                                    <?php echo date('Y-m-d',strtotime($vo['post_date'])); ?>
                                </span>
                                <a href="<?php echo leuu('article/index',array('id'=>$vo['object_id'],'cid'=>$vo['term_id']));?>"><?php echo ($vo["post_title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="main-right">
        <div class="products">
            <h3 class="category-title category-color-1">Products</h3>
            <?php $index_pro=sp_getslide("index_pro",2); ?>
            <?php if(is_array($index_pro)): foreach($index_pro as $key=>$vo): ?><dl>
                    <dt><a href="<?php echo ($vo["slide_url"]); ?>"><img src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" alt=""></a></dt>
                    <dd><a href="<?php echo ($vo["slide_url"]); ?>"><?php echo ($vo["slide_des"]); ?></a></dd>
                </dl><?php endforeach; endif; ?>

        </div>
        <div class="products-list">
            <h3 class="category-title category-color-2">主要产品列表</h3>
            <?php $terms = sp_get_child_terms(15); $ids = array(15); ?>
            <?php if(is_array($terms)): foreach($terms as $key=>$vo): $ids[] = $vo['term_id']; ?>
                <?php $termss = sp_get_child_terms($vo['term_id']); ?>
                <?php if(is_array($termss)): foreach($termss as $key=>$voo): $ids[] = $voo['term_id']; endforeach; endif; endforeach; endif; ?>
<?php $ids=implode(',',$ids); $lists = sp_sql_posts_paged("order:post_date DESC;where:recommended=1 and term_id in($ids);limit:0,15",0); ?>
            <ul>
                <?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="#"><?php echo (msubstr($vo["post_title"],0,18,'utf-8')); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="footer">
    <p>上海凯倍达电子科技有限公司 版权所有   技术支持：慧聪国际资讯 慧聪行业门户网</p>
</div>
</body>
<script>
    jQuery(".slideBox").slide({mainCell:".bd ul",autoPlay:true,effect:"left"});
</script>
</html>